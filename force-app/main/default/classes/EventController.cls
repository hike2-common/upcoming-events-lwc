public with sharing class EventController {
    @AuraEnabled(Cacheable=true)
    public static Event[] getAllEvents() {
        return [
            SELECT
                Id,
                Title__c,
                DurationInMinutes,
                Subject,
                Description,
                Location,
                StartDateTime,
                EndDateTime
            FROM Event
            WHERE StartDateTime > YESTERDAY
            AND Subject = 'Meeting'
            ORDER BY StartDateTime
            LIMIT 90
        ];
    }
}